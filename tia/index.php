<!DOCTYPE html>
<html lang="en">
<head>

    <?php
    include "view/main.php";
    ?>

    <script src="https://use.fontawesome.com/5ef57c5097.js"></script>


    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" />
    <title>Paper Bootstrap Wizard by Creative Tim</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- CSS Files -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />


    <!-- CSS Just for demo purpose, don't include it in your project -->



    <!-- Fonts and Icons -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/themify-icons.css" rel="stylesheet">

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>

    <!--  Plugin for the Wizard -->
    <script src="assets/js/paper-bootstrap-wizard.js" type="text/javascript"></script>

    <!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
    <script src="assets/js/jquery.validate.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/main.css">
    <script src="js/view_fill.js"></script>


    </head>

    <body>

        <div id="black"></div>
        <div id="wrapper">
            <header>
                <i onclick="fill_editor();" class="fa fa-user-circle-o" aria-hidden="true"></i>
                <i onclick="fill_menu();" class="fa fa-home" aria-hidden="true"></i>
            </header>

            <section>
                <div id="fill_div"></div>



            </section>
        </div>

    </body>

<script>
    fill_menu();
</script>

</html>